KBTRIS: the keyboard lover's block puzzle game
==============================================

A standard Tetris(C)(TM)(BYOB) board is 10 by 20 squares.  
Cool.  
Your keyboard's homerow has 10+ buttons you can push super-fast.  
Cool.  
Your keyboard has additional rows you can also push very fast.  
Cool.  
Instead of left-left-left-left-rotate-drop, KDTRIS lets you type 'w'.  
Cool?  Cool.  Cool cool cool.  

Play on the web:  
http://kbtris.penduin.net/


Default Controls
----------------

Pressing a homerow key moves the tetrad to the indicated column and orients it
to its default rotation at the same time.  Note the cirle-marked pivot point
of the tetrad -- that's the bit which will end up in the column you type.

The row above the homerow moves the tetrad and orients it counterclockwise.  
The row below moves and orients clockwise.  
Number keys move and orient to 180 degrees.  

Space drops the tetrad instantly.  Escape pauses.

         pause: esc
    orient 180:  1 2 3 4 5 6 7 8 9 0
    orient ccw:  q w e r t y u i o p
          move:  a s d f g h j k l ;
     orient cw:  z x c v b n m , . /
          drop:       [ space ]



Advanced Moves
--------------

Once you get the hang of positioning tetrads, try auto-drop mode, where every
keystroke instantly drops the piece after applying the position and rotation.

As in most versions of Tetris(R)(BS)(WTF) it is possible to scoot a tetrad
even after it lands.  The trick here is to press the second key before letting
up on the first.  If you prefer, scoot mode may be disabled.

With scoot turned off and auto-drop turned on, every press of every key will
instantly land the current tetrad and the next one will become available,
enabling the fastest possible play at the cost of scoot moves and any shred
of safety.


License
-------

GPLv3.  See COPYING for details.  
Music is public domain.  (Thanks, archive.org!)  


From the Author
---------------

Have fun!  There's a learning curve here, and my hope is that keyboard-loving
Tetris fans like myself will get as much out of this game as they put into it.

I would be very interested and gratified to see somebody play this game very
well and very fast.  What's the highest lines-per-minute you can achieve?

(I mean without cheating, but if you want to flex a completely different set
of skills, sure, I'd be interested to see how people break this too.  :^)

- Owen <owenswerk@gmail.com>

PS. This isn't the first time I've ruined Tetris.  Check out TONG!  
https://gitlab.com/smogheap/tong
