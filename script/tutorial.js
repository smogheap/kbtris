var TUT = {
	text: [
		"Welcome to KBtris!  This guide shows how to play.",
		"Everybody knows Tetris, right?",
		"Let's say you have this arrangement.",
		"Normally, you'd go:                                 ",
		"Normally, you'd go: left                            ",
		"Normally, you'd go: left-left                       ",
		"Normally, you'd go: left-left-left                  ",
		"Normally, you'd go: left-left-left-left             ",
		"Normally, you'd go: left-left-left-left-rotate      ",
		"Normally, you'd go: left-left-left-left-rotate-drop.",
		"In KBtris, you go: 'q'-drop'",
		"First, let's look at the home row.",
		"Each key lines up with a column.",
		"See the center bit of the tetrad marked with a circle?",
		"That bit will end up in the column you type.",
		"One key can move you anywhere instantly!",
		"Space bar drops the piece and gives you the next one.",
		"Now, for the other rows of keys!",
		"See the rotation guide on the left?",
		"Those are the orientations you'll get from each row.",
		"The orientation you want,                           ",
		"The orientation you want, and the column you want...",
		"Are coordinates for the key to type!",
		"It will take some time and practice to learn...",
		"But it will let you play very, very fast!",
		"Like most versions of Tetris, you can scoot landings.",
		"Press the movement key while still holding spacebar.",
		"You can get very efficient playing this way.",
		"However, you can go even faster!",
		"Once you can hit the right key each time...",
		"Turn on 'Auto-Drop' and quit using the space bar!",
		"Every single keystroke orients, moves, AND drops.",
		"You can still scoot landings, too...",
		"Hold the first key while typing the second.",
		"Pretty intense, right!",
		"Now, for MAXIMUM SPEED!",
		"If you turn off 'Scoot'...",
		"KBtris will not wait for you to release a keystroke.",
		"The instant you type a key, you get your next piece.",
		"There is no safety net, only unforgiving speed.",
		"But that's getting ahead of ourselves!",
		"To begin, leave 'Auto-drop' off and 'Scoot' on.",
		"In 'Practice' mode, tetrads won't drop by themselves.",
		"How many lines per minute (LPM) can you get?",
		"Keep practicing, and have fun!"
	]
};
