var canvas = document.querySelector("#bg");
var w = 160;
var h = 90;
canvas.width = w;
canvas.height = h;
var ctx = canvas.getContext("2d");

var time = Date.now();
var buffer = ctx.createImageData(w, h);

function render(time, fragcoord) {
    var u = [(fragcoord[0] / w)*2-1,(fragcoord[1] / w)*2-(h/w)];
    var t = time*0.2;
    var tt = Math.sin(t/8)*64;
    var x = u[0]*tt+Math.sin(t*2.1)*4.0;
    var y = u[1]*tt+Math.cos(t*2.3)*4.0;
    var c = Math.sin(x)+Math.sin(y);
    var zoom = Math.sin(t);
    x = x*zoom*2.0+Math.sin(t*1.1);
    y = y*zoom*2.0+Math.cos(t*1.3);
    var xx = Math.cos(t*0.7)*x-Math.sin(t*0.7)*y;
    var yy = Math.sin(t*0.7)*x+Math.cos(t*0.7)*y;
    c=(Math.sin(c+(Math.sin(xx)+Math.sin(yy)))+1.0)*0.4;
    var v = 2-Math.sqrt(u[0]*u[0]+u[1]*u[1])*2;
    return [v*(c+v*0.4),v*(c*c-0.5+v*0.5),v*(c*1.9),1];
};

function animate() {
    var delta = (Date.now() - time) / 1000;
    buffer = ctx.createImageData(w, h);
    ctx.clearRect(0, 0, w, h);
    for (var x = 0; x < w; x++) {
        for (var y = 0; y < h; y++) {
            var ret = render(delta, [x, y]);
            var i = (y * buffer.width + x) * 4;
            buffer.data[i] = ret[0] * 255;
            buffer.data[i + 1] = ret[1] * 255;
            buffer.data[i + 2] = ret[2] * 255;
            buffer.data[i + 3] = ret[3] * 255;
        }
    }
    ctx.putImageData(buffer, 0, 0);
    requestAnimationFrame(animate);
};

animate();
